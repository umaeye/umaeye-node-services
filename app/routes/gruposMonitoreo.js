module.exports = function(app) {
    // Models
    var GrupoMonitoreo = require('../models/grupoMonitoreo');

    // Routes
    GrupoMonitoreo.methods(['get', 'put', 'post', 'delete']);
    GrupoMonitoreo.register(app, '/gruposMonitoreo');
};