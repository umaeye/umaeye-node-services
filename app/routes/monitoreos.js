module.exports = function(app) {
    // Models
    var Monitoreo = require('../models/monitoreo');
    var ItemClasificadorGeneral = require('../models/itemClasificadorGeneral');
    var ItemClasificador = require('../models/itemClasificador');

    // Routes
    Monitoreo.methods(['get', 'put', 'post', 'delete']);

    Monitoreo.after('post', function(req, res, next) {
        console.log(res.locals.bundle);
        var date = new Date(res.locals.bundle.fechaMuestreo);

        Monitoreo.findOneAndUpdate (
        {_id: res.locals.bundle._id},
        {$set: {
            year: date.getFullYear()
        }}, function(err, doc){
            if(err){
                console.log("Something wrong when updating data!");
                res.send({message: 'Error!'});
                return;
            }
        });

        ItemClasificadorGeneral.find({}, '-_id -__v', function (err, itemsClasificadorGeneral) {
            if(!err) {
                itemsClasificadorGeneral = addParamsMonitoreo(
                    res.locals.bundle._id,
                    res.locals.bundle.cuenca,
                    res.locals.bundle.puntoMonitoreo,
                    res.locals.bundle.fechaMuestreo,
                    itemsClasificadorGeneral
                );

                // console.log(itemsClasificadorGeneral);

                ItemClasificador.insertMany(itemsClasificadorGeneral, function(err, res) {
                    if(!err) {
                        console.log('insertado Correcto');
                    } else {
                        console.log('Error insert items clasificador general to items clasificador' + err);
                    }
                });
            } else {
                console.log('Error get items clasificador general' + err);
            }
        });
        next();
    });

    Monitoreo.route('getByDay', {
        detail: false,
        methods: ['get'],
        handler: function(req, res, next) {
            var fechaMuestreo = new Date(req.query['fechaMuestreo']);
            var fechaMuestreolt = new Date(req.query['fechaMuestreo']);
            fechaMuestreolt.setDate(fechaMuestreo.getDate() + 1);

            Monitoreo.find({'fechaMuestreo': {
                '$gte': fechaMuestreo,
                '$lt': fechaMuestreolt
            }}, function(err, monitoreos) {
                if(!err) {
                    res.send(monitoreos);
                } else {
                    res.send({message: 'Error!'});
                }

            });
        }
    });

    Monitoreo.route('getByDateRange', {
        detail: false,
        methods: ['get'],
        handler: function(req, res, next) {
            var fechaMuestreogt = new Date(req.query['fechaMuestreogt']);
            var fechaMuestreolt = new Date(req.query['fechaMuestreolt']);

            Monitoreo.find({'fechaMuestreo': {
                '$gte': fechaMuestreogt,
                '$lt': fechaMuestreolt
            }}, function(err, monitoreos) {
                if(!err) {
                    res.send(monitoreos);
                } else {
                    res.send({message: 'Error!'});
                }

            });
        }
    });

    Monitoreo.route('getByYear', {
        detail: false,
        methods: ['get'],
        handler: function(req, res, next) {
            var yeargte = new Date(req.query['year']);
            var yearlt = new Date((parseInt(req.query['year']) + 1).toString());

            Monitoreo.find({'fechaMuestreo': {
                '$gte': yeargte,
                '$lt': yearlt
            }}, function(err, monitoreos) {
                if(!err) {
                    res.send(monitoreos);
                } else {
                    res.send({message: 'Error!'});
                }

            });
        }
    });

    Monitoreo.route('getYears', {
        detail: false,
        methods: ['get'],
        handler: function(req, res, next) {
            Monitoreo.find().distinct('year', function(err, years) {
                if(!err) {
                    var jsonRes = [];
                    years.forEach(function (element, index, array){
                        jsonRes.push({year: element})
                    });

                    res.send(jsonRes);
                } else {
                    res.send({message: 'Error!'});
                }
            });
        }
    });

    Monitoreo.register(app, '/monitoreos');
};

function addParamsMonitoreo(idMonitoreo, cuenca, puntoMonitoreo, fechaMuestreo, items) {
    var fecha = new Date(fechaMuestreo);
    items.forEach(function (element, index, array){
        element.monitoreo = idMonitoreo;
        element.cuenca = cuenca;
        element.puntoMonitoreo = puntoMonitoreo;
        element.fechaMuestreo = fecha;
        element.year = fecha.getFullYear();
    });

    return items;
};