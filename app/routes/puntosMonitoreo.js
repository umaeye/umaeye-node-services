module.exports = function(app) {
    // Models
    var PuntoMonitoreo = require('../models/puntoMonitoreo');

    // Routes
    PuntoMonitoreo.methods(['get', 'put', 'post', 'delete']);

    PuntoMonitoreo.register(app, '/puntosMonitoreo');
};