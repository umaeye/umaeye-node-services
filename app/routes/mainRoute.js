module.exports = function(app) {
    // Routes
    var classificationRoute = require('./classification')(app);
    var cuencaRoute = require('./cuencas')(app);
    var puntoMonitoreoRoute = require('./puntosMonitoreo')(app);
    var monitoreoRoute = require('./monitoreos')(app);
    var gruposMonitoreoRoute = require('./gruposMonitoreo')(app);
    var itemsClasificadorRoute = require('./itemsClasificador')(app);
    var itemsClasificadorGeneralRoute = require('./itemsClasificadorGeneral')(app);
};