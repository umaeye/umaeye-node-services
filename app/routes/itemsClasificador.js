module.exports = function(app) {
    // Models
    var Monitoreo = require('../models/monitoreo');
    var ItemClasificador = require('../models/itemClasificador');
    var PuntoMonitoreo = require('../models/puntoMonitoreo');
    var q = require('q');

    // Routes
    ItemClasificador.methods(['get', 'put', 'post', 'delete']);

    ItemClasificador.route('getCountsMonitoreo', {
        detail: false,
        methods: ['get'],
        handler: function(req, res, next) {

            var idMonitoreo = req.query['idMonitoreo'] || null;
            var cuenca = req.query['cuenca'] || null;
            var puntoMonitoreo = req.query['puntoMonitoreo'] || null;
            var year = req.query['year'] || null;
            var idGrupo = req.query['idGrupo'] || null;

            // console.log(getCountCondition('A', idMonitoreo, cuenca, puntoMonitoreo, year, idGrupo));

            q.all([
                ItemClasificador.count(getCountCondition('A', idMonitoreo, cuenca, puntoMonitoreo, year, idGrupo)).exec(),
                ItemClasificador.count(getCountCondition('B', idMonitoreo, cuenca, puntoMonitoreo, year, idGrupo)).exec(),
                ItemClasificador.count(getCountCondition('C', idMonitoreo, cuenca, puntoMonitoreo, year, idGrupo)).exec(),
                ItemClasificador.count(getCountCondition('D', idMonitoreo, cuenca, puntoMonitoreo, year, idGrupo)).exec(),
                ItemClasificador.count(getCountCondition('CRT', idMonitoreo, cuenca, puntoMonitoreo, year, idGrupo)).exec(),
                ItemClasificador.count(getCountCondition('', idMonitoreo, cuenca, puntoMonitoreo, year, idGrupo)).exec(),
                ItemClasificador.count(getCountCondition(null, idMonitoreo, cuenca, puntoMonitoreo, year, idGrupo)).exec(),
            ]).then(function(response) {
                res.send([{
                    countA: response[0],
                    countB: response[1],
                    countC: response[2],
                    countD: response[3],
                    countCRT: response[4],
                    countSC: response[5],
                    countTotal: response[6]
                }]);
            })
            .catch(function(err){
                // just need one of these
                res.send({message: 'error'});
            });
        }
    });

    ItemClasificador.register(app, '/itemsClasificador');
};

function getCountCondition(typeClass, idMonitoreo, cuenca, puntoMonitoreo, year, idGrupo) {
    var condition = {};

    if(typeClass) {
        condition.clase = typeClass;
    } else {
        if(typeClass === '') {
            condition.clase = '';
        }
    }

    if(idMonitoreo) {
        condition.monitoreo = idMonitoreo;
    }

    if(cuenca) {
        condition.cuenca = cuenca;
    }

    if(puntoMonitoreo) {
        condition.puntoMonitoreo = puntoMonitoreo;
    }

    if(year) {
        condition.year = year;
    }

    if(idGrupo) {
        condition.idGrupo = idGrupo;
    }

    return condition;
};