module.exports = function(app) {
    // Models
    var ItemClasificadorGeneral = require('../models/itemClasificadorGeneral');

    // Routes
    ItemClasificadorGeneral.methods(['get', 'put', 'post', 'delete']);
    ItemClasificadorGeneral.register(app, '/itemsClasificadorGeneral');
};