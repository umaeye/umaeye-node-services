module.exports = function(app) {
    // Models
    var Cuenca = require('../models/cuenca');

    // Routes
    Cuenca.methods(['get', 'put', 'post', 'delete']);
    Cuenca.register(app, '/cuencas');
};