module.exports = function(app) {
    // Models
    var Classification = require('../models/classification');

    // Routes
    Classification.methods(['get', 'put', 'post', 'delete']);
    Classification.register(app, '/classification');
};