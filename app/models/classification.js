var restful = require('node-restful');
var mongoose = require('mongoose');

var Classification = mongoose.Schema({
        codigo: 'string',
        nombre: 'string',
        departamento: 'string',
        municipio: 'string',
        comunidad: 'string',
        latitud_s: 'string',
        longitud_s: 'string',
        altura: 'string',
        fecha_muestreo: 'string',
        hora_muestreo: 'string',
        caudal: 'string',
        ancho_rio: 'string',
        punto_muestreo: 'string'
    });

module.exports = restful.model('Classification', Classification);