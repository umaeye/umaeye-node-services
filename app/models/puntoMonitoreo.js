var restful = require('node-restful');
var mongoose = require('mongoose');

var PuntoMonitoreo = mongoose.Schema({
    codigo: 'string',
    nombre: 'string',
    departamento: 'string',
    municipio: 'string',
    comunidad: 'string',
    altura: 'string',
    latitud: 'string',
    longitud: 'string',
    este: 'string',
    norte: 'string',
    zonaUtm: 'string',
    cuenca: { type: mongoose.Schema.ObjectId, ref: "Cuenca" }
});

module.exports = restful.model('PuntoMonitoreo', PuntoMonitoreo);