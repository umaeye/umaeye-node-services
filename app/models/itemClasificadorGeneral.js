var restful = require('node-restful');
var mongoose = require('mongoose');

var ItemClasificadorGeneral = mongoose.Schema({
    parametro: 'string',
    unidad: 'string',
    valor: 'string',
    clase: 'string',
    minA: 'string',
    maxA: 'string',
    ruleMinA: 'string',
    ruleMaxA: 'string',
    minB: 'string',
    maxB: 'string',
    ruleMinB: 'string',
    ruleMaxB: 'string',
    minC: 'string',
    maxC: 'string',
    ruleMinC: 'string',
    ruleMaxC: 'string',
    minD: 'string',
    maxD: 'string',
    ruleMinD: 'string',
    ruleMaxD: 'string',
    criticMin: 'string',
    criticMax: 'string',
    ruleCriticMin: 'string',
    ruleCriticMax: 'string',
    fiftyPercent: 'string',
    cuenca: 'string',
    puntoMonitoreo: 'string',
    monitoreo: 'string',
    fechaMuestreo: 'string',
    year: 'string',
    idGrupo: 'string'
});

module.exports = restful.model('ItemClasificadorGeneral', ItemClasificadorGeneral);