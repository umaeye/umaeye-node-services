var restful = require('node-restful');
var mongoose = require('mongoose');
var FormatDate = mongoose.Schema.Types.FormatDate = require('mongoose-schema-formatdate');

var Monitoreo = mongoose.Schema({
    fechaMuestreo: { type: Date },
    year: 'string',
    horaMuestreo: 'string',
    caudal: 'string',
    anchoRio: 'string',
    puntoMuestreo: 'string',
    responsableInforme: 'string',
    responsableMuestreo: 'string',
    responsableAnalisis: 'string',
    responsableInformacion: 'string',
    recursosPropiosAsignados: 'string',
    cuenca: { type: mongoose.Schema.ObjectId, ref: "Cuenca" },
    puntoMonitoreo: { type: mongoose.Schema.ObjectId, ref: "PuntoMonitoreo" }
});

module.exports = restful.model('Monitoreo', Monitoreo);