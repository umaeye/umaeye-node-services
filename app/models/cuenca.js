var restful = require('node-restful');
var mongoose = require('mongoose');

var Cuenca = mongoose.Schema({
    nombre: 'string',
    descripcion: 'string'
});

module.exports = restful.model('Cuenca', Cuenca);