var restful = require('node-restful');
var mongoose = require('mongoose');

var ItemClasificador = mongoose.Schema({
    fechaMuestreo: { type: Date },
    year: 'string',
    parametro: 'string',
    unidad: 'string',
    valor: 'string',
    clase: 'string',
    minA: 'string',
    maxA: 'string',
    ruleMinA: 'string',
    ruleMaxA: 'string',
    minB: 'string',
    maxB: 'string',
    ruleMinB: 'string',
    ruleMaxB: 'string',
    minC: 'string',
    maxC: 'string',
    ruleMinC: 'string',
    ruleMaxC: 'string',
    minD: 'string',
    maxD: 'string',
    ruleMinD: 'string',
    ruleMaxD: 'string',
    criticMin: 'string',
    criticMax: 'string',
    ruleCriticMin: 'string',
    ruleCriticMax: 'string',
    fiftyPercent: 'string',
    idGrupo: 'string',
    cuenca: { type: mongoose.Schema.ObjectId, ref: "Cuenca" },
    puntoMonitoreo: { type: mongoose.Schema.ObjectId, ref: "PuntoMonitoreo" },
    monitoreo: { type: mongoose.Schema.ObjectId, ref: "Monitoreo" },
    grupoMonitoreo: { type: mongoose.Schema.ObjectId, ref: "GrupoMonitoreo" }
});

module.exports = restful.model('ItemClasificador', ItemClasificador);