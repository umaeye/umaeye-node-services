var restful = require('node-restful');
var mongoose = require('mongoose');

var GrupoMonitoreo = mongoose.Schema({
    nombre: 'string',
    countA: 'number',
    countB: 'number',
    countC: 'number',
    countD: 'number',
    countCrt: 'number',
    countTotal: 'number',
    idGrupo: 'string'
});

module.exports = restful.model('GrupoMonitoreo', GrupoMonitoreo);